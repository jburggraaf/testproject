/*
 *  memory_leak
 *
 *  This is a simple program that contains a memory leak.  It simply sits in
 *  an infinite loop doing calls to malloc() but never calls free().
 *
 *  Its command line is:
 *
 *    memory_leak [-v] [-ssize].. [-tsleeptime]
 *
 *  Example - if run as follows, it will allocate 16 bytes + 1024 bytes every
 *  2.5 seconds
 *
 *    memory_leak
 *
 *  Example - if run as follows, it will allocate 400 bytes + 10 bytes +
 *  4000 bytes every .5 seconds (500 milliseconds).  It will also be verbose
 *  and tell you before it does each allocation.
 *
 *    memory_leak -s400 -s10 -s4000 -t500 -v
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void options (int argc, char **argv);
void elaborate_leak( void );
void function1( void ** ptr);
void function2( void ** ptr);
void function3( void ** ptr);
void function4( void ** ptr);

#define MAXNUMSIZES 5
int nsizes, sizes[MAXNUMSIZES];	/* sizes of mallocs to do */
struct timespec sleeptime;			/* time to sleep between mallocs in
									   milliseconds */

char *progname;
int verbose = 0;

int
main(int argc, char *argv[])
{
	int i;
	void * aptr;
	int n = 10;

	progname = argv[0];

    setvbuf(stdout, NULL, _IOLBF, BUFSIZ );

    printf ("%s:  starting...\n", progname);

    options (argc, argv);

	while (n--) {
		for (i = 0; i < nsizes; i++) {
			if (verbose)
				printf ("%s:  mallocing %d bytes\n", progname, sizes[i]);
			if ((aptr = malloc (sizes[i])) == NULL) {
				fprintf (stderr, "%s:  Out of memory\n", progname);
				exit (EXIT_FAILURE);
        	}
        	elaborate_leak();
		}
		nanosleep (&sleeptime, NULL);
	}

	return EXIT_SUCCESS;
}

/*
 *  options
 *
 *  This routine handles the command line options.
*/

void
options (int argc, char **argv)
{
    int     opt;

	nsizes = 0;

	/* default is 2.5 seconds */
	sleeptime.tv_sec = 2;
	sleeptime.tv_nsec = 500000000; /* 500,000,000 nsec = 500 msec = .5 sec */

    while ((opt = getopt (argc, argv, "s:t:v")) != -1) {
        switch (opt) {
		case 's':
			if (nsizes == MAXNUMSIZES) {
				fprintf (stderr, "%s:  Too many sizes given.  Maximum is %d\n",
					progname, MAXNUMSIZES);
				exit (EXIT_FAILURE);
        	}
        	sizes[nsizes] = atoi (optarg);
        	nsizes++;
            break;
        case 't':
        	sleeptime.tv_sec = atoi (optarg) / 1000;
        	sleeptime.tv_nsec = atoi (optarg) % 1000;
        	break;
        case 'v':
        	verbose = 1;
        	break;
        }
    }

    if (nsizes == 0) {
		nsizes = 2;
		sizes[0] = 16;
		sizes[1] = 1024;
    }
}

/* more elaborate leak */
void elaborate_leak( void ) {
	void *ptr;

	function1( &ptr );
}

void function1( void ** ptr) {
	function2( ptr );
}

void function2( void ** ptr) {

	*ptr = malloc( 1024 );
	function3( ptr );
}

void function3( void ** ptr) {
	function4( ptr );
}

void function4( void ** ptr) {
	if ((rand() % 2) == 0) {
		free (*ptr);
	} else {
		printf ("no free!\n");
	}
}

