#include <cstdlib>
#include <iostream>
#include <time.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include "PersonStruct.h"

using namespace std;

struct person{
    string name;
    int age;
    float rating;
};

string int_to_string(int i){
    string s = static_cast<ostringstream*>( &(ostringstream() << i) )->str();
    return s;
}

string float_to_string(float f){
    string s = static_cast<ostringstream*>( &(ostringstream() << f) )->str();
    return s;
}

void growUp(person *p){
    p->age += 1;
//    char* chout = &p->name[0];
//    printf("%s is getting older!\n", chout);
//    cout << p->name << " is getting older!" << endl;
//    cout.flush();
}

void changeName(person *p, string newName){
    string output = p->name + " is changing names to " + newName + "!!";
    p->name = newName;
//    char* chout = &output[0];
//    printf("%s \n", chout);
//    cout << output << endl;
//    cout.flush();
}

void getBetter(person *p){
    p->rating += 0.7;
//    char* chout = &p->name[0];
//    printf("%s is getting better!", chout);
//    cout << p->name << " is getting better!" << endl;
//    cout.flush();
}

void getWorse(person *p){
    p->rating -= 0.4;
//    char* chout = &p->name[0];
//    printf("%s is getting worse!", chout);
//    cout << p->name << " is getting worse!" << endl;
//    cout.flush();
}

void printPerson(person *p){
    string output = "";
    output += "This person: " + p->name + " ";
    if (p->age < 30){
        output += "is really young and good looking (at age "+ int_to_string(p->age) +"). ";
    }else{
        output += "is on their last legs (at age "+ int_to_string(p->age) +"). ";
    }

    if (p->rating < 3){
        output += "Most people think that " + p->name + " is pretty terrible. Rated "+ float_to_string(p->rating) +".";
    }else{
        output += "Everyone agrees that " + p->name + " is super cool. Rated "+ float_to_string(p->rating) +".";
    }

//    char* chout = &output[0];
//    printf("%s \n", chout);
//    cout << output << endl;
//    cout.flush();
}

int main(int argc, char *argv[]) {

	int count = 1;

	srand(time(NULL));
    person* jon = new person();
    person* ash = new person();

    jon->name = "Jon";
    jon->age = 27;
    jon->rating = 3.4;
    printPerson(jon);

    ash->name = "Ash";
    ash->age = 25;
    ash->rating = 4.2;
    printPerson(ash);

    while(count){
        int number = rand()%100;

        if (number % 8 == 0){
            getBetter(ash);
        }else if (number % 8 == 1){
            getBetter(jon);
        }else if (number % 8 == 2){
            getWorse(ash);
        }else if (number % 8 == 3){
            getWorse(jon);
        }else if (number % 8 == 4){
            growUp(ash);
        }else if (number % 8 == 5){
            growUp(jon);
        }else if (number % 8 == 6){
            changeName(ash, "ash." + int_to_string(number));
        }else if (number % 8 == 7){
            changeName(jon, "jon." + int_to_string(number));
        }

        if (count == 1000){
        	try{
            	throw 20;
            }
            catch (int e){
//            	cout << "An exception occurred. Exception Nr. " << e << endl;
//            	cout.flush();
            }
        }

//        printPerson(ash);
//        printPerson(jon);
        int millisecondsSleep = 20;
        int sleeptime = millisecondsSleep*1000;
        usleep(sleeptime);
        count++;
    }
    return EXIT_SUCCESS;
}

